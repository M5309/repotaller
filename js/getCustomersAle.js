var clientesObtenidos;
function getCustomersAle() {

  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function functionName() {
    if (this.readyState == 4 && this.status == 200) {
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarclientes();

    }
  }
  request.open("GET",url,true);
  request.send();
  }

function procesarclientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].CompanyName;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;
    var columnaBandera = document.createElement("td");

    var imgBandera = document.createElement ("img");
    imgBandera.classList.add("flag");

    if (JSONClientes.value[i].Country == "UK") {
      imgBandera.src = rutaBandera + "United-Kingdom.png"
    } else {
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }
    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
